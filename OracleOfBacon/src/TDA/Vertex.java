/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TDA;

import java.util.LinkedList;
import java.util.Objects;

/**
 *
 * @author eduardo
 * @param <E>
 */
public class Vertex <E>{
    private final E data;
    private final LinkedList<Edge<E>> edges;
    private boolean visited;
    private int distancia;
    private Vertex<E> antecesor;    
    public Vertex(E data) {
        edges=new LinkedList<>();
        this.data = data;
        distancia=Integer.MAX_VALUE;
    }
    public E getData() {
        return data;
    }
    public LinkedList<Edge<E>> getEdges() {
        return edges;
    }
    public void setVisited(boolean visited) {
        this.visited = visited;
    }
    public boolean isVisited() {
        return visited;
    }
    public void setDistancia(int distancia) {
        this.distancia = distancia;
    }
    public void setAntecesor(Vertex<E> antecesor) {
        this.antecesor = antecesor;
    }
    public int getDistancia() {
        return distancia;
    }
    public Vertex<E> getAntecesor() {
        return antecesor;
    }    
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Vertex<E> other = (Vertex<E>) obj;
        return Objects.equals(this.data, other.data);
    }
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 67 * hash + Objects.hashCode(this.data);
        return hash;
    }
}