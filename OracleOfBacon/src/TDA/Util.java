/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TDA;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Josue
 */
public class Util {
    public static GraphLA leerArchivo(){
        GraphLA graph = new GraphLA(false);
        try(BufferedReader b = new BufferedReader(new FileReader("src\\files\\data.txt"))){//"src\\files\\data.txt" leera el archivo que tiene solo 500 datos, "data.txt" leera el archivo completo
            String cadena;
            int linea = 1;
            while((cadena = b.readLine())!=null){
                String[] datos = cadena.split(":");
                String m = datos[1].replace("\"", "").replace(",cast","");
                String[] actores= datos[2].replace("[", "").replace("]","").replace("\"", "").replace(",directors","").split(",");
                ArrayList actorObjetos = new ArrayList<>();
                for(String s: actores){
                    graph.addVertex(s);
                    actorObjetos.add(s);
                }
                for(int i = 0;i<actorObjetos.size();i++){
                    for(int z = i;z<actorObjetos.size();z++){
                        if(!(i==z)){
                            graph.addEdge(actorObjetos.get(i),actorObjetos.get(z), m);
                        }
                    }
                }
            }
        }catch(IOException ex){
            Logger.getLogger(Util.class.getName()).log(Level.SEVERE, null, ex);
        }
        return graph;        
    }
}