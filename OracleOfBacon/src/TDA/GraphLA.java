/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TDA;

import java.util.Deque;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Stack;

/**
 *
 * @author eduardo
 */
public class GraphLA <E>{
    private LinkedList<Vertex<E>> vertexes;
    private boolean directed;
    
    public GraphLA(boolean directed){
        this.directed = directed;
        vertexes = new LinkedList<>();
    }
    
    public boolean addVertex(E data){
        Vertex<E> v = new Vertex<>(data);
        return (data == null || vertexes.contains(v))?false:vertexes.add(v);
    }
    
    public boolean removeVertex(E data){
        if(data == null || vertexes.isEmpty()) return false;
        ListIterator<Vertex<E>> iv = vertexes.listIterator();
        while(iv.hasNext()){
            Vertex<E> v = iv.next();
            ListIterator<Edge<E>> ie = v.getEdges().listIterator();
            while(ie.hasNext()){
                Edge<E> e = ie.next();
                if(e.getVDestino().getData().equals(data))
                    ie.remove();
            }
        }
        Vertex<E> vi = new Vertex<>(data);
        return vertexes.remove(vi);
    }    
    public boolean addEdge(E src, E dst,String s){
        if(src == null || dst == null) return false;
        Vertex<E> vs = searchVertex(src);
        Vertex<E> vd = searchVertex(dst);
        if(vs == null || vd == null) return false;
        Edge<E> e = new Edge<>(vs,vd,s);
        if(!vs.getEdges().contains(e))
            vs.getEdges().add(e);
        if(!directed){
            Edge<E> ei = new Edge<>(vd,vs,s);
            if(!vd.getEdges().contains(ei))
                vd.getEdges().add(ei);
        }
        return true;
    }
    
    public boolean removeEdge(E src, E dst){
        if(src == null || dst == null) return false;
        Vertex<E> vs = searchVertex(src);
        Vertex<E> vd = searchVertex(dst);
        if(vs == null || vd == null) return false;
        Edge<E> e = new Edge<>(vs,vd,"");
        vs.getEdges().remove(e);
        if(!directed){
            e = new Edge<>(vd,vs,"");
            vd.getEdges().remove(e);
        }
        return true;
    }
    
    private Vertex<E> searchVertex(E data){
        for(Vertex<E> v : vertexes)
        {
            if(v.getData().equals(data))
                return v;
        }
        return null;
    }

    //recorrido por anchura
    public HashMap<E,E> bfs(E data){
        HashMap<E,E> result=new HashMap<E,E>();
        if(data==null) return result;
        Vertex<E> v= searchVertex(data);
        if(v==null) return result;
        String m = new String();
        v.setVisited(true);
        Queue<Vertex<E>> cola= new LinkedList<>();
        Queue<String> cola2 = new LinkedList<>();
        cola.offer(v);
        cola2.offer(m);
        while(!cola.isEmpty()){
            v=cola.poll();
            m=cola2.poll();
            result.put(v.getData(),(E) m);
            for(Edge<E> e: v.getEdges()){
                if(e.getVDestino().isVisited()){
                    e.getVDestino().setVisited(true);
                    cola.offer(e.getVDestino());
                    cola2.offer(m);
                }                   
            }
        }
        cleanVertex();
        return result;
    }
    
    private void cleanVertex(){
        for(Vertex<E> v:vertexes){            
            v.setVisited(false);
            v.setDistancia(Integer.MAX_VALUE);
            v.setAntecesor(null);
        }
    }
    
    public HashMap<E,E> dfs(E data){
        HashMap<E,E> result=new HashMap<E,E>();
        if(data==null) return result;
        Vertex<E> v= searchVertex(data);
        if(v==null) return result;
        String m = new String();
        v.setVisited(true);
        Deque<Vertex<E>> pila= new LinkedList<>();
        Deque<String> pila2= new LinkedList<>();
        pila.push(v);
        pila2.push(m);
        while(!pila.isEmpty()){
            v=pila.pop();
            m = pila2.pop();
            result.put(v.getData(),(E) m);
            for(Edge<E> e: v.getEdges()){
                if(e.getVDestino().isVisited()){
                    e.getVDestino().setVisited(true);
                    pila.push(e.getVDestino());
                    pila2.push(e.getPelicula());
                }                   
            }
        }
        cleanVertex();
        return result;
    }
    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        for (Vertex<E> v:vertexes){            
            LinkedList<Edge<E>> listadeEdges=v.getEdges();
            if(!listadeEdges.isEmpty()){
                for (Edge e: listadeEdges){
                        sb.append("(");
                        sb.append(e.getVOrigen().getData()+",");
                        sb.append(e.getVDestino().getData()+",");
                }
            }
        }
        if(sb.length()>1){
            int ult=sb.lastIndexOf(",");
            sb.deleteCharAt(ult);
            sb.append("}");
            return sb.toString();
        }else{
            sb.append("}");
            return sb.toString();
        }
    }
    private void dijkstra(E inicio){
        Vertex<E> v=searchVertex(inicio);
        if(v!=null){
            PriorityQueue<Vertex<E>> cola=new PriorityQueue<>((Vertex<E> v1,Vertex<E> v2)->v1.getDistancia()-v2.getDistancia());
            v.setDistancia(0);
            cola.offer(v);
            while(!cola.isEmpty()){
                v=cola.poll();
                v.setVisited(true);
                for(Edge<E> e:v.getEdges()){
                    if(!e.getVDestino().isVisited()){
                        if(v.getDistancia()+1<e.getVDestino().getDistancia()){
                            e.getVDestino().setAntecesor(v);
                            e.getVDestino().setDistancia(v.getDistancia()+1);
                            cola.offer(e.getVDestino());                       
                        }
                    }
                }
            }
        }
    }
    public int menorDistancia(E inicio, E fin){
        dijkstra(inicio);
        Vertex<E> v=searchVertex(fin);
        if(inicio==null || fin==null || searchVertex(inicio)==null || v==null)return -1;
        if(inicio.equals(fin))return 0;        
        int t=v.getDistancia();
        cleanVertex();
        return t;
    }
    public List<E> caminoMinimo(E inicio, E fin){
        List<E> l=new LinkedList<>();
        dijkstra(inicio);
        Vertex<E> v=searchVertex(fin);
        if(inicio==null || fin==null || searchVertex(inicio)==null || v==null)return l;        
        Stack<E> s= new Stack<>();        
        s.add(v.getData());
        while(!v.getData().equals(inicio)){
            Iterator<Edge<E>> it=v.getEdges().iterator();
            Edge<E> e=it.next();
            while(it.hasNext() && !e.getVDestino().getData().equals(v.getAntecesor().getData()))
                e=it.next();
            s.add((E)e.getPelicula());
            s.add(v.getAntecesor().getData());
            v=v.getAntecesor();
        }
        while(!s.isEmpty())
            l.add(s.pop());        
        cleanVertex();
        return l;
    }
}