/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oracleofbacon;

import GUI.VentanaPrincipal;
import TDA.GraphLA;
import TDA.Util;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author Josue
 */
public class OracleOfBacon extends Application{
    private static final GraphLA g=Util.leerArchivo();
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        /* Pruebas
        Carlos Lozano   -   Ian Abercrombie
        Bruce Campbell  -   Fernando Fernán Gómez
        Anna Lizaran    -   Núria Espert
        Penélope Cruz   -   Antonia San Juan
        Candela Peña    -   Richard Grove
        */
        launch(args);
    }
    public static GraphLA getGrafo() {
        return g;
    }
    @Override
    public void start(Stage primaryStage){
        Scene s= new Scene(new VentanaPrincipal().getRoot(),1000,600);
        primaryStage.setTitle("The oracle of Bacon");
        primaryStage.setScene(s);
        primaryStage.show();}
}