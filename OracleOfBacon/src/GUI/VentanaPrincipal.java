/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import TDA.GraphLA;
import java.util.List;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollBar;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import oracleofbacon.OracleOfBacon;

/**
 *
 * @author Josue
 */
public class VentanaPrincipal {   
    private final BorderPane root = new BorderPane();
    private final TextField solicitud1= new TextField();
    private final TextField solicitud2= new TextField();
    private final Label lblSup=new Label();
    private final Label lblInf=new Label(" to ");
    private final Button btnFind=new Button("Find link");
    private final VBox center=new VBox();
    private final VBox top=new VBox();
    private final HBox bottom=new HBox();
   public VentanaPrincipal(){
       bottom.setPadding(new Insets(20,20,25,20));
       bottom.setAlignment(Pos.CENTER);
       bottom.setSpacing(15);
       bottom.getChildren().addAll(solicitud1, lblInf, solicitud2, btnFind);
       top.setPadding(new Insets(0,0,25,0));
       top.setAlignment(Pos.CENTER);
       top.setSpacing(15);
       center.setAlignment(Pos.CENTER);
       root.setCenter(bottom);
       ScrollBar vertical = new ScrollBar();
       vertical.setOrientation(Orientation.VERTICAL);
       vertical.setMin(0);
       vertical.setMax(root.getHeight()+220);
       vertical.valueProperty().addListener((ObservableValue<? extends Number> ov, Number t, Number t1) -> {
           root.setLayoutY(-t1.doubleValue());
       });
       root.setRight(vertical);
       btnFind.setOnAction(e->{
            if(solicitud1.getText()!=null && solicitud2.getText()!=null)
                 mostrarRelacion(solicitud1.getText(),solicitud2.getText());
       });
    }
    public BorderPane getRoot(){        
        return root;
    }
    private void mostrarRelacion(String s1, String s2){
        GraphLA g = OracleOfBacon.getGrafo();
        long startTime = System.nanoTime();
        int i = OracleOfBacon.getGrafo().menorDistancia(s1, s2);
        List<String> s = OracleOfBacon.getGrafo().caminoMinimo(s1, s2);  
        long endTime = System.nanoTime();
        long duration = (endTime - startTime)/1000000;
        Label dfs = new Label("Tiempo de ejecucion de djisktra: "+duration+ " milisegundos.");
        top.getChildren().clear();
        lblSup.setText(s2+" has a "+s1+" number of "+i);
        top.getChildren().addAll(lblSup,dfs); 
        mostrarGrafico(s);
        root.setTop(top);
        root.setCenter(center);
        root.setBottom(bottom);
    }
    private void mostrarGrafico(List<String> l){
        if(l.size()>0){
            center.getChildren().clear();    
            for(int i=0;i<l.size();i++){
                HBox hb=new HBox();
                hb.setAlignment(Pos.CENTER);
                Label t=new Label();
                Button b=new Button(l.get(i));
                b.setPadding(new Insets(5, 15, 5, 15));
                if(i%2==0){
                    b.setStyle("-fx-background-color: #68A5E1; -fx-font-weight: bold; -fx-font-size: 15;");
                    t.setText("was in");
                }else{ 
                    b.setStyle("-fx-background-color: #7FEABB; -fx-font-weight: bold; -fx-font-size: 15;");
                    t.setText("with");
                }
                Line li=new Line();
                li.setEndY(li.getStartY()+30);
                li.setStrokeWidth(2);
                li.setStroke(Color.GREY);
                hb.getChildren().addAll(li,t);
                if(i==l.size()-1)center.getChildren().add(b);
                else center.getChildren().addAll(b,hb);
            }
        }
    }
}